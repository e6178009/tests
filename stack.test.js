const s = require('./stack.js');
let stack = null;

function initialiser(){
  stack = new s.Stack();

}

beforeEach(() => {
  initialiser();

});


test('count() retourne 0 si la pile est vide', () => {

  //Act
  const compte = stack.count();

  //Assert
  expect(compte).toEqual(0);

});