const ex = require('./exemples.js');

test('additionner 1 + 2 égale 3', () => {
	expect(ex.somme(1, 2)).toEqual(3);
});

test('susceptible lance toujours des exceptions', () => {
	// Assert
	expect(() => {
	ex.susceptible();
	}).toThrow(Error);
});